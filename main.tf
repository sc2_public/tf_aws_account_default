#
# Set up a regular Stanford AWS account
#

# ----- Account Alias -----

#
# Create an account alias, which is displayed on the AWS SSO page
# (after authentication)
#
resource "aws_iam_account_alias" "alias" {
  account_alias = var.alias
}


# ----- Password Policy -----

#
# Set up the password policy for IAM accounts
#
resource "aws_iam_account_password_policy" "default" {
  minimum_password_length        = var.minimum_password_length
  max_password_age               = var.max_password_age
  require_lowercase_characters   = var.require_lowercase_characters
  require_numbers                = var.require_numbers
  require_uppercase_characters   = var.require_uppercase_characters
  require_symbols                = var.require_symbols
  allow_users_to_change_password = var.allow_users_to_change_password
  password_reuse_prevention      = var.password_reuse_prevention
}

# ----- SAML IdP and Roles -----

#
# Get the current SAML IdP metadata
#
data "external" "metadata" {
  program = [ "/bin/bash", "-c", "jq --arg m \"$(curl -s ${var.metadata_url})\" -n '{metadata:$m}'" ]
}

#
# Create the IdP in IAM
#
resource "aws_iam_saml_provider" "idp" {
  name                   = var.idp_name
  saml_metadata_document = data.external.metadata.result.metadata
}


#
# Create a policy for assuming roles via SAML
#
data "aws_iam_policy_document" "saml-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRoleWithSAML"]

    principals {
      type        = "Federated"
      identifiers = [ aws_iam_saml_provider.idp.arn ]
    }

    condition {
      test = "StringEquals"
      variable = "SAML:aud"
      values = [
        "https://signin.aws.amazon.com/saml"
      ]
    }
  }
}

#
# Create roles that can be assumed via SAML SSO,
# and attach suitable policies
#
resource "aws_iam_role" "saml_roles" {
  count = length(var.saml_roles)
  name  = var.saml_roles[count.index]
  assume_role_policy = data.aws_iam_policy_document.saml-assume-role-policy.json
}

resource "aws_iam_role_policy_attachment" "saml_role_policies" {
  count      = length(var.saml_roles)
  role       = aws_iam_role.saml_roles.*.name[count.index]
  policy_arn = lookup(var.saml_role_policies, var.saml_roles[count.index])
}

#
# Create service-linked role for AWS Config
#
resource "aws_iam_service_linked_role" "config" {
  aws_service_name = "config.amazonaws.com"
}

