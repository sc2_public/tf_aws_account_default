#
# Only alias needs to be defined by the caller; everything else has sensible defaults
#

variable "alias" {
  description = "Alias for AWS account - shown next to account ID on SSO page"
  type        = string
}

variable "minimum_password_length" {
  description = "Minimum password length for IAM Password Policy"
  type        = number
  default     = 8
}

variable "max_password_age" {
  description = "Maximum password age for IAM Password Policy"
  type        = number
  default     = 365
}

variable "password_reuse_prevention" {
  description = "Number of previous passwords that cannot be reused for IAM Password Policy"
  type        = number
  default     = 5
}

variable "require_lowercase_characters" {
  description = "Require lowercase characters in password"
  type        = bool
  default     = true
}

variable "require_uppercase_characters" {
  description = "Require uppercase characters in password"
  type        = bool
  default     = true
}

variable "require_numbers" {
  description = "Require numbers in password"
  type        = bool
  default     = true
}

variable "require_symbols" {
  description = "Require symbols in password"
  type        = bool
  default     = true
}

variable "allow_users_to_change_password" {
  description = "Allow users to change their password"
  type        = bool
  default     = true
}

variable "metadata_url" {
  description = "URL for IdP metadata"
  type        = string
  default     = "https://login.stanford.edu/idp/shibboleth"
}

variable "metadata_file" {
  description = "Filename for downloaded IdP metadata"
  type        = string
  default     = "stanford-idp.xml"
}

variable "idp_name" {
  description = "IdP IAM Name - used in IdP ARN"
  type        = string
  default     = "stanford-idp"
}

variable "saml_roles" {
  description = "List of SAML-related roles that will be created"
  type = list
  default = [
    "admin",
    "billing",
    "operations"
  ]
}


variable "saml_role_policies" {
  description = "Mapping of role names to AWS policy ARNs"
  type        = map
  default     = {
    "admin"           = "arn:aws:iam::aws:policy/AdministratorAccess"
    "billing"         = "arn:aws:iam::aws:policy/job-function/Billing"
    "operations"      = "arn:aws:iam::aws:policy/PowerUserAccess"
  }
}

