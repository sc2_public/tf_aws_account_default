output "alias" {
  value = aws_iam_account_alias.alias.account_alias
}

output "config_service_role_arn" {
  value = aws_iam_service_linked_role.config.arn
}
