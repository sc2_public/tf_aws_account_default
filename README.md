# tf_aws_account_default

Sets up a default Stanford AWS account. To do this, it

* Creates roles for `admin`, `billing`, and `operations`
* Registers the Stanford IdP with the account to allows mapping from
  Workgroup memberships to AWS roles
* Defines an alias for the account, which makes it easier to select
  the correct account / role during SSO if a person has access to
  multiple accounts
* Configures the IAM password policy
* Creates a Service Linked Role for AWS Config

## Variables

| Name                             | Type   | Description                                                                | Default                                                                           |
|----------------------------------|--------|----------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| `alias`                          | string | alias for AWS account                                                      | _none_                                                                            |
| `metadata_url`                   | string | URL for IdP metadata                                                       | https://login.stanford.edu/idp/shibboleth                                         |
| `idp_name`                       | string | Name for IAM SAML Provider                                                 | stanford-idp                                                                      |
| `saml_roles`                     | list   | List of SAML-assumed roles to create                                       | admin,<br/>billing,<br/>operations                                                |
| `saml_role_policies`             | list   | Map from role name to policy                                               | admin: AdministratorAccess,<br/>billing: Billing,<br/>operations: PowerUserAccess |
| `minimum_password_length`        | number | Minimum password length for IAM Password Policy                            | 8                                                                                 |
| `max_password_age`               | number | Maximum password age for IAM Password Policy (days)                        | 365                                                                               |
| `password_reuse_prevention`      | number | Number of previous passwords that cannot be reused for IAM Password Policy | 5                                                                                 |
| `require_lowercase_characters`   | bool   | Require lowercase characters in password                                   | true                                                                              |
| `require_uppercase_characters`   | bool   | Require uppercase characters in password                                   | true                                                                              |
| `require_numbers`                | bool   | Require numbers in password                                                | true                                                                              |
| `require_symbols`                | bool   | Require symbols in password                                                | true                                                                              |
| `allow_users_to_change_password` | bool   | Allow users to change their password                                       | true                                                                              |

## Outputs

| Name                      | Type   | Description                            |
|---------------------------|--------|----------------------------------------|
| `alias`                   | string | Generated account alias                |
| `config_service_role_arn` | string | Service linked role ARN for AWS Config |

## Usage

This module should only be instantiated once per account, and will run in the default region.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "foo-prod"
}
```

## Related Modules

* [AWS Config Setup](https://code.stanford.edu/sc2_public/tf_aws_account_config/)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
* [Guardduty setup](https://code.stanford.edu/sc2_public/tf_aws_account_guardduty)
* [Qualys CloudView setup](https://code.stanford.edu/sc2_public/tf_aws_account_qualys)
